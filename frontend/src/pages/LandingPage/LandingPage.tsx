import React from 'react';
import './LandingPage.css';

function LandingPage() {
  return (
    <div className="LandingPage">
      <h1>Welcome to CleanBooking!</h1>
      <p>Book your cleaning appointment today!</p>
    </div>
  );
}

export default LandingPage;
